<?php

include_once("config.php");

if(isset($_POST['update']))
{
    $id = $_POST['id'];
    $project_name=$_POST['project_name'];
    $client=$_POST['client'];
    $project_leader=$_POST['project_leader'];
    $start=$_POST['start'];
    $end=$_POST['end'];
    $progress=$_POST['progress'];

    $result = mysqli_query($mysqli, "UPDATE project_monitoring SET project_name='$project_name', client='$client', project_leader='$project_leader', start='$start', end='$end', progress='$progress' WHERE id=$id");

    header("Location: index.php");
}
?>
<?php

$id = $_GET['id'];

$result = mysqli_query($mysqli, "SELECT * FROM project_monitoring WHERE id=$id");

while($data = mysqli_fetch_array($result))
{
    $project_name = nl2br($data['project_name']);
    $client = $data['client'];
    $project_leader = $data['project_leader'];
    $start = $data['start'];
    $end = $data['end'];
    $progress = $data['progress'];

}
?>
<html>
<head>
    <title>Edit Project</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
    <h1 class="p-3 mb-2 bg-info text-white text-center">Project Monitoring</h1>

    <a type="button" class="btn btn-primary p-2 m-2" href="index.php">Back</a>

    <form name="update_user" method="post" action="edit.php">
        <table class="table ml-4">
            <tr>
                <td>Project Name</td>
                <td><input class="form-control" type="text" name="project_name" value= "<?php echo $project_name; ?>" ></td>
            </tr>
            <tr>
                <td>Client</td>
                <td><input class="form-control" type="text" name="client" value= "<?php echo $client; ?>" ></td>
            </tr>
            <tr>
                <td>Project Leader</td>
                <td><input class="form-control" type="text" name="project_leader" value= "<?php echo $project_leader; ?>" ></td>
            </tr>
            <tr>
                <td>Start</td>
                <td><input class="form-control" type="date" name="start" value= "<?php echo $start; ?>"" ></td>
            </tr>
            <tr>
                <td>End</td>
                <td><input class="form-control" type="date" name="end" value= "<?php echo $end; ?>" ></td>
            </tr>
            <tr>
                <td>Progress</td>
                <td><input class="form-control" type="number" name="progress" value= "<?php echo $progress; ?>" ></td>
            </tr>
            <tr>
                <td><input type="hidden" name="id" value= <?php echo $_GET['id']; ?> ></td>
                <td><input class="btn btn-success p-2 m-2" type="submit" name="update" value="Update"></td>
            </tr>
        </table>
    </form>
</body>
</html>