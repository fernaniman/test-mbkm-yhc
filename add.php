<html>
<head>
    <title>Add Project</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
    <h1 class="p-3 mb-2 bg-info text-white text-center">Project Monitoring</h1>

    <a type="button" class="btn btn-primary p-2 m-2" href="index.php">Back</a>

    <form action="add.php" method="post" name="form1">
        <table class="table ml-4">
            <tr>
                <td>Project Name</td>
                <td><input class="form-control" type="text" name="project_name"></td>
            </tr>
            <tr>
                <td>Client</td>
                <td><input class="form-control" type="text" name="client"></td>
            </tr>
            <tr>
                <td>Project Leader</td>
                <td><input class="form-control" type="text" name="project_leader"></td>
            </tr>
            <tr>
                <td>Start</td>
                <td><input class="form-control" type="date" name="start"></td>
            </tr>
            <tr>
                <td>End</td>
                <td><input class="form-control" type="date" name="end"></td>
            </tr>
            <tr>
                <td>Progress</td>
                <td><input class="form-control" type="number" name="progress" placeholder="0-100"></td>
            </tr>
            <tr>
                <td></td>
                <td><input class="btn btn-success p-2 m-2" type="submit" name="Submit" value="Add"></td>
            </tr>
        </table>
    </form>

    <?php

    if(isset($_POST['Submit'])) {
        $project_name = $_POST['project_name'];
        $client = $_POST['client'];
        $project_leader = $_POST['project_leader'];
        $start = $_POST['start'];
        $end = $_POST['end'];
        $progress = $_POST['progress'];


        include_once("config.php");

        $result = mysqli_query($mysqli, "INSERT INTO project_monitoring(project_name, client, project_leader, start, end, progress) VALUES('$project_name', '$client', '$project_leader', '$start', '$end', '$progress')");

        header("Location: index.php");
    }
    ?>
</body>
</html>