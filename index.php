<?php

// koneksi database
include_once("config.php");

// memanggil data dari database
$result = mysqli_query($mysqli, "SELECT * FROM project_monitoring ORDER BY id DESC");
?>

<html>
<head>
    <title>Project Monitoring</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
    <h1 class="p-3 mb-2 bg-info text-white text-center">Project Monitoring</h1>

    <a type="button" class="btn btn-primary p-2 m-2" href="add.php">Add Project</a>

    <table class="table table-hover table-info" width='80%' border=1>
    <tr class="bg-info text-center">
        <th scope="row">No</th>
        <th scope="row">Project Name</th>
        <th scope="row">Client</th>
        <th scope="row">Project Leader</th>
        <th scope="row">Start</th>
        <th scope="row">End</th>
        <th scope="row">Progress</th>
        <th scope="row">Update</th>
    </tr>
    <?php
    $no = 0;
    while($data = mysqli_fetch_array($result)) {
        $no+=1;
        echo "<tr>";
        echo "<td>".$no."</td>";
        echo "<td>".$data['project_name']."</td>";
        echo "<td>".$data['client']."</td>";
        echo "<td>".$data['project_leader']."</td>";
        echo "<td>".$data['start']."</td>";
        echo "<td>".$data['end']."</td>";
        echo "<td>
            <div class='progress'>
                <div class='progress-bar' role='progressbar' style='width:".$data['progress']."%' aria-valuemin='0' aria-valuemax='100'></div>
            </div>
            <div class='text-center'>".$data['progress']."%</div>
            </td>";
        echo "<td>
            <a class='btn btn-primary' href='edit.php?id=$data[id]'>Edit</a>
            <a class='btn btn-danger' href='delete.php?id=$data[id]'>Delete</a>
        </td></tr>";
    }
    ?>
    </table>
</body>
</html>