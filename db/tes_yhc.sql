-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 17 Jul 2022 pada 22.17
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tes_yhc`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `project_monitoring`
--

CREATE TABLE `project_monitoring` (
  `id` int(11) NOT NULL,
  `project_name` varchar(100) NOT NULL,
  `client` varchar(50) NOT NULL,
  `project_leader` varchar(50) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `progress` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `project_monitoring`
--

INSERT INTO `project_monitoring` (`id`, `project_name`, `client`, `project_leader`, `start`, `end`, `progress`) VALUES
(1, 'Website Rumah Sakit', 'RS ABC', 'Lisandro Martinez', '2022-07-10', '2022-07-24', 70),
(2, 'Web Profile', 'PT DEF', 'Christian Eriksen', '2022-07-24', '2022-08-07', 60);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `project_monitoring`
--
ALTER TABLE `project_monitoring`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `project_monitoring`
--
ALTER TABLE `project_monitoring`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
